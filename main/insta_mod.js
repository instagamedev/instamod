// Enable copy paste into node-webkit
// var gui = require('nw.gui');
// win = gui.Window.get();
// var nativeMenuBar = new gui.Menu({ type: "menubar" });
// try {
// nativeMenuBar.createMacBuiltin("My App");
// win.menu = nativeMenuBar;
// } catch (ex) {
// console.log(ex.message);
// }


(function(){
    var originalShowReviewWindow = UI.showReviewWindow;

    var instaReview = function(b, c){
        var d = $("#reviewWindow");
        d.find("#reviewAnimationContainer").empty();
        d.find(".okButton").hide().clickExclOnce(function() {
            g = [];
            k = [];
            UI.closeModal(c)
        });
        UI.showModalContent("#reviewWindow", {
            disableCheckForNotifications: !0,
            onOpen: function() {
                instaOpen();
            },
            onClose: function() {
                GameManager.company.activeNotifications.remove(b)
            }
        })

    };

    UI.showReviewWindow = instaReview;

    var instaOpen = function() {
        // console.log('--instaOpen--');
        console.log(process.versions['node-webkit']);
        var $simple_modal = $(".simplemodal-data"),
            $reviewAnimation = $simple_modal.find("#reviewAnimationContainer"),
            $reviews_template = $("#reviewItemTemplate"),
            gm = GameManager.company.gameLog.last(),
            modal_title = "Reviews for {0}".localize().format(gm.title),
            $window_title = $simple_modal.find(".windowTitle").text(modal_title);

            gm.reviews.forEach(function(element, index, array){
                $cloneReview = $reviews_template.clone();
                $cloneReview.removeAttr("id");
                $cloneReview.scoreElement = $cloneReview.find(".score").text(element.score);
                $cloneReview.textElement = $cloneReview.find(".text").text(element.message).css('min-width', 160);
                $cloneReview.reviewerElement = $cloneReview.find(".reviewer").text('... ' + element.reviewerName);
                $cloneReview.award = $cloneReview.find(".award").css('opacity', (element.score >= 10)? 1 : 0 );

                $stars = $cloneReview.find('.stars');
                $stars.find('.star').css('display', 'none');

                if (element.score % 2 === 0) {
                    var $s = $stars.find('.star.full');
                    $res = $s.slice(0, Math.floor(element.score/2));
                    $res.css('display', 'inline');
                } else {
                    if(element.score == 1) {
                        $stars.find('.star.half:first').css('display', 'inline-block');
                    } else {
                        var $s = $stars.find('.star.full');
                        $res = $s.slice(0, Math.floor(element.score/2));
                        $res.css('display', 'inline-block');
                        setTimeout(function() {
                            $stars.find('.star.full:visible:last').next('.star.half').css('display', 'inline-block');
                        }, 0);

                    }
                }

                $cloneReview.css('margin-left', 120);
                if (index !== 0) {
                    $cloneReview.css('margin-top', 120 * index);
                }
                $reviewAnimation.append($cloneReview)
            });

            $simple_modal.find(".okButton").css('margin', '0 auto').slideDown();
    };

    /*
    instaConvention v1.0
    mod for GameDev Tycoon
    author: Adrien Verhulst
    Description: makes the convention window instant
    */
    var speedInstantenaousConvention = 10;
    (function() {
        var a = ["./images/superb/conf1f.png", "./images/superb/conf2f.png", "./images/superb/conf3f.png"], b = "./images/superb/conff1r.png ./images/superb/conff2r.png ./images/superb/conff3r.png ./images/superb/conff4r.png ./images/superb/confm1r.png ./images/superb/confm2r.png ./images/superb/confm3r.png ./images/superb/confm4r.png".split(" "), c = "./images/superb/conff1l.png ./images/superb/conff2l.png ./images/superb/conff3l.png ./images/superb/conff4l.png ./images/superb/confm1l.png ./images/superb/confm2l.png ./images/superb/confm3l.png ./images/superb/confm4l.png".split(" ");
        UI.showConferenceBoothList = function(a, b) {
            var c = GameManager.company.booths;
            GameManager.company.conferenceStandFactor = null;
            $(".selectionOverlayContainer").hide();
            var d = $("#conferenceBoothPicker"), f = {disableCheckForNotifications: !0,close: !0,onClose: function() {
                    GameManager.company.activeNotifications.remove(a);
                    GameManager.resume(!0);
                    b && b()
                }};
            d.find(".conferenceBoothPickerText").text("The big game convention will take place in four weeks time. Do you want to participate?".localize());
            var g = d.find(".conferenceBoothSliderContainer");
            g.empty();
            var h = $('<div class="conferenceBoothVariationContainer royalSlider rsDefaultInv"></div>');
            g.append(h);
            for (var g = $("#genericBoothTemplate"), k = 0; k < c.length; k++) {
                var l = c[k];
                if ("" !== l.description) {
                    var n = g.clone();
                    n.find(".boothTitle").text(l.name);
                    n.find(".boothDescription").text(l.description);
                    n.find(".boothCashCost").text("Costs: {0}".localize().format(UI.getShortNumberString(l.cost)));
                    h.append(n)
                }
            }
            PlatformShim.ISWIN8 ? h.gdSlider() : f.onOpen = function() {
                h.gdSlider()
            };
            d.find(".okButton").clickExcl(function() {
                Sound.click();
                var a = d.find(".rsActiveSlide").find(".boothTitle").text(), b = GameManager.company.booths.first(function(b) {
                    return b.name == a
                });
                GameManager.company.cash >= b.cost ? (GameManager.company.conferenceStandFactor = b.standFactor, GameManager.company.adjustCash(-b.cost, b.name), UI.closeModal()) : d.find(".centeredButtonWrapper").effect("shake", {times: 2,distance: 5}, 50)
            });
            UI.showModalContent("#conferenceBoothPicker", f)
        };
        var d = function(a, b) {
            var c = "", c = GameManager.company.currentGame ? b ? GameManager.company.gameLog.last().title :
            GameManager.company.currentGame.title : b ? GameManager.company.gameLog[GameManager.company.gameLog.length - 2].title : GameManager.company.gameLog.last().title, d = 150, f = 50, h = 378, k = 369;
            2 === a ? b ? (d = 90, f = 25, h = 385, k = 392) : (d = 140, f = 15, h = 387, k = 274) : 3 === a ? (d = 245, f = 33, h = 372, k = 234) : 4 === a && (b ? (d = 176, f = 40, h = 379, k = 304) : (d = 150, f = 15, h = 378, k = 252));
            return g(c, d, f, h, k)
        }, f = function(a, b) {
            var c = GameManager.company.name, d = 180, f = 30, h = 381, k = 256;
            2 === a ? (d = 255, f = 35, h = 382, k = 237) : 3 === a ? (d = 300, f = 35, h = 373, k = 180, b && (d = 127, f = 15, h = 376, k = 331)) :
            4 === a && (d = 290, f = 29, h = 378, k = 348);
            return g(c, d, f, h, k)
        }, g = function(a, b, c, d, f) {
            var g = new createjs.Container;
            g.x = d;
            g.y = f;
            d = 32;
            do
                f = "bold {0}pt {1}".format(d, "Segoe UI"), f = new createjs.Text(a, f, "black"), d -= 1;
            while ((f.getMeasuredWidth() > b || f.getMeasuredLineHeight() > c) && 1 < d);
            b = d / 32;
            c = createjs.Graphics.getHSL(0, 0, 24);
            f = new createjs.Text(a, "bold 32pt {0}".format("Segoe UI"), c);
            f.textAlign = "center";
            f.textBaseline = "middle";
            g.scaleX = b;
            g.scaleY = b;
            g.addChild(f);
            return g
        }, k = function(a, b) {
            var c = "", d = !1;
            GameManager.currentHwProject &&
            GameManager.currentHwProject.announced ? (c = GameManager.currentHwProject.iconUri, d = !0) : (c = GameManager.company.licencedPlatforms.last(function(a) {
                return a.isCustom
            })) ? (c = c.iconUri, d = !0) : c = GameManager.company.currentGame ? Platforms.getPlatformImage(GameManager.company.currentGame.platforms[0], GameManager.company.currentWeek) : Platforms.getPlatformImage(GameManager.company.gameLog.last().platforms[0], GameManager.company.currentWeek);
            if (b && !d)
                for (d = GameManager.company.gameLog.length - 1; 0 < d; d--) {
                    var f = Platforms.getPlatformImage(GameManager.company.gameLog[d].platforms[0],
                    GameManager.company.currentWeek);
                    if (f != c) {
                        c = f;
                        break
                    }
                }
            c = new createjs.Bitmap(c);
            d = new createjs.Container;
            d.scaleX = 0.2;
            d.scaleY = 0.2;
            3 === a ? (d.x = 356, d.y = 350) : 2 === a ? (d.y = 315, d.x = b ? 265 : 460) : 4 === a && (d.scaleX = 0.08, d.scaleY = 0.08, d.y = 300, d.x = b ? 236 : 509);
            d.addChild(c);
            return d
        };
        UI._showGameConferenceAnimation = function(a, b) {
            var c = $("#gameConferenceAnimationDialog"), g = GameManager.company.booths.first(function(a) {
                return a.standFactor == GameManager.company.conferenceStandFactor
            });
            c.find(".windowTitle").text(4 != g.id ?
            "Game Convention: G3".localize("heading") : "{0} Convention".localize("heading").format(GameManager.company.name));
            if (g) {
                c.find(".conventionImageFloor").attr("src", g.floorImage);
                c.find(".conventionImageBg").attr("src", g.bgImage);
                var h = c.find(".conventionImageFg");
                g.fgImage ? (h.show(), h.attr("src", g.fgImage)) : h.hide()
            }
            c.find(".okButton").unbind("click").click(UI.closeConferenceAnimationDialog).hide();
            var l = this;
            UI.showModalContent("#gameConferenceAnimationDialog", {disableCheckForNotifications: !0,onOpen: function() {
                    var b =
                    c.find("#animationLayer");
                    b.empty();
                    l.stage = new createjs.Stage(b[0]);
                    l.stage.canvas.height = l.stage.canvas.clientHeight;
                    l.stage.canvas.width = l.stage.canvas.clientWidth;
                    l.flippingBox = new FlippingCounter.FlippingBox(8, 6);
                    l.flippingBox.init();
                    l.flippingBox.fill("00000000");
                    l.flippingBox.container.x = 41;
                    l.flippingBox.container.y = 41;
                    b = new createjs.Container;
                    b.addChild(FlippingCounter.panel);
                    b.addChild(l.flippingBox.container);
                    b.x = 190;
                    b.scaleX = 0.4;
                    b.scaleY = 0.4;
                    l.stage.addChild(b);
                    l.stage.addChild(f(g.id));
                    l.stage.addChild(d(g.id));
                    if (3 === g.id)
                        l.stage.addChild(f(g.id, !0)), l.stage.addChild(k(g.id));
                    else if (2 === g.id || 4 === g.id)
                        l.stage.addChild(d(g.id, !0)), l.stage.addChild(k(g.id)), l.stage.addChild(k(g.id, !0));
                    UI._startGameConferenceAnimations(a.buttonText, g.id)
                },onClose: function() {
                    GameManager.company && -1 != GameManager.company.activeNotifications.indexOf(a) && GameManager.company.activeNotifications.remove(a);
                    var c = a.buttonText, d = c / 1E6 * 0.1 + 1, f = GameManager.company;
                    f.currentGame && (f.currentGame.hypePoints +=
                    Math.min(200, Math.floor(c / 1E6 * 200)));
                    for (var g = Sales.getGamesToSell(f), h = 0; h < g.length; h++)
                        void 0 === g[h].confAmount ? g[h].totalSalesCash *= d : 0 < g[h].confAmount && (g[h].totalSalesCash += g[h].confAmount * (d - 1)), g[h].confAmount = 0;
                    g = Sales.getConsolesToSell(f);
                    for (h = 0; h < g.length; h++)
                        g[h].unitsSold += 1.5 / Sales.consoleUnitPrice * (c / 1E6);
                    d = Math.floor(f.fans * (d - Math.floor(d)) / 10);
                    0 < d && f.adjustFans(d);
                    f.conferenceHype = Math.min(200, Math.floor(c / 1E6 * 200));
                    b && b()
                },close: !1})
        };
        UI.closeConferenceAnimationDialog = function() {
            UI.closeModal()
        };
        var l = [], h = 0.9;
        GameManager.addTickListener(function(a) {
            if (l && 0 < l.length) {
                a *= h;
                for (var b = 0; b < l.length; b++)
                    l[b].tick(a, !1);
                UI.stage.update()
            }
        });
        var m = function() {
            h = 1.4
        }, n = [], p = [], q = [];
        UI._startGameConferenceAnimations = function(d, f) {
            if (0 === n.length) {
                for (var g = 0; g < a.length; g++)
                    n.push(new createjs.Bitmap(a[g]));
                for (g = 0; g < c.length; g++)
                    q.push(new createjs.Bitmap(c[g]));
                for (g = 0; g < b.length; g++)
                    p.push(new createjs.Bitmap(b[g]))
            }
            var k = $(".simplemodal-data"), w = k.find(".animationLayer");
            l = [];
            FlippingCounter._activeUITweens =
            l;
            h = 0.9 * speedInstantenaousConvention;
            $(window).on("click", m);
            var y = 1200;

            l.push(createjs.Tween.get(w).wait(0).call(function() {
                UI.flippingBox.fill(0)
            }));
            l.push(createjs.Tween.get(w).wait(y).call(function() {
                UI.flippingBox.fill(Math.round(d / 20))
            }));
            l.push(createjs.Tween.get(w).wait(2 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 15))
            }));
            l.push(createjs.Tween.get(w).wait(3 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 10))
            }));
            l.push(createjs.Tween.get(w).wait(4 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 9))
            }));
            l.push(createjs.Tween.get(w).wait(5 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 6))
            }));
            l.push(createjs.Tween.get(w).wait(6 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 4))
            }));
            l.push(createjs.Tween.get(w).wait(7 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 2.2))
            }));
            l.push(createjs.Tween.get(w).wait(8 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 1.8))
            }));
            l.push(createjs.Tween.get(w).wait(9 * y).call(function() {
                UI.flippingBox.fill(Math.round(d / 1.2))
            }));
            l.push(createjs.Tween.get(w).wait(10 * y).call(function() {
                UI.flippingBox.fill(d)
            }));

            var B = UI.stage.canvas.width, v = UI.stage.canvas.height, x = 5E3, E = 6E4 * y / d;
            3 === f ? (x /= 2, E /= 2) : 4 === f && (x /= 2.5, E /= 2.5);
            y = E;
            for (g = 0; g <= d; g += x) {
                var y = y + E, A = s(B, v, f);
                UI.stage.addChild(A);
                l.push(createjs.Tween.get(A).wait(y).to({alpha: 0.15*speedInstantenaousConvention}, 500))
            }

            g = createjs.Tween.get(w).wait(y).call(function() {
                k.find(".okButton").slideDown("fast");
                $(window).off("click", m);
                l = [];
                FlippingCounter._activeUITweens = l;
                h = 0.9 * speedInstantenaousConvention
            });

            l.push(g)
        };
        var s = function(a, b, d) {
            var c, f, g, h, k;
            1 === d ? (k = 1, c = 130 +
            Math.random() * (a - 260 - 62 * k), f = 35 * Math.random() + 415 - 176 * k, g = 283, h = 474) : 2 === d ? (k = 0.625, c = 100 + Math.random() * (a - 200 - 62 * k), f = 30 * Math.random() + 420 - 176 * k, g = 233, h = 531) : 3 === d ? (k = 0.075, f = Math.random(), c = 113 + f * (a - 205 - 62 * k), k *= 4 * Math.abs(0.46 - f) + 1, f = 412 - 176 * k, g = 338, h = 413) : 4 === d && (k = 0.15, f = Math.random(), k *= 4 * Math.abs(0 - f) + 1, c = Math.random() * (a - 62 * k), f = 85 * f + 365 - 176 * k, g = 283, h = 476);
            a = void 0;
            a = 3 > d && c < g || 4 === d && c < g || 3 === d && c > h ? q.pickRandom().clone() : 3 > d && c > h || 4 === d && c > h || 3 === d && c > g ? p.pickRandom().clone() : n.pickRandom().clone();
            a.alpha = 0;
            a.x = c;
            a.y = f;
            a.scaleX = k;
            a.scaleY = k;
            return a
        }
    })();



    /*
    instaTypewriter v1.0
    mod for GameDev Tycoon
    author: Adrien Verhulst
    Description: makes the tipewriter effect instant
    */
    var instantenaousTypeWriter = 1; // If you want default behaviour, change 1 by 0.

    (function(a) {
        var b = [], c;
        GameManager.addTickListener(function(a) {
            if (0 != b.length) {
                if (isNaN(c) || 0 === c)
                    c = 1;
                a *= c;
                if (0 !== a)
                    for (var f = 0; f < b.length; f++)
                        b[f].tick(a, !1)
            }
        }, !1);
        a.fn.typewrite = function(d) {
            d || (d = {});
            var f = Localization.isRTLLanguage(), g = {selector: this,delay: 100,speedUpOnClick: !1,callback: null,animateScroll: !0,scrollPadding: 25,scrollPollIntervalInChars: 50};
            d && a.extend(g, d);
            f && (g.delay *= 5, g.scrollPollIntervalInChars /= 5);
            c = 1;
            var k = function(a) {
                c = (c + 2.5).clamp(1, 10)
            };
            if (d.speedUpOnClick) {
                var l = g.callback;
                g.callback = function() {
                    a(window).off("click", k);
                    l()
                }
            }
            for (var h = a(g.selector), m = h.text().replaceAll("<br />", "\n"), n = a("<span></span>"), p = {}, q = "", s = 0; s < m.length; s++)
                if (q += m[s], "[" == q) {
                    var t = m.substr(s);
                    t.startsWith("[pause:") ? (q = s + 7, s = m.indexOf("]", q), q = m.substr(q, s - q), p[n.children().length] = {type: "pause",value: parseFloat(q)}, q = "") : t.startsWith("[delay:") && (q = s + 7, s = m.indexOf("]", q), q = m.substr(q, s - q), p[n.children().length] = {type: "delay",value: q}, q = "")
                } else
                    "\n" == q ? (n.append(a("<br />")), q = "") : f ? " " ==
                    m[s] && (n.append(a("<span></span>").text(q)), q = "") : (n.append(a("<span></span>").text(q)), q = "");
            "" != q && n.append(a("<span></span>").text(q));
            var r = [];
            n.children().each(function() {
                a(this).css({opacity: 0})
            });
            h.empty();
            h.append(n);
            var u = 0, z = 0, w = 0, y = n.children().length, B = h.parent();
            n.children().each(function(b) {
                (function(a, b) {
                    r.push(function() {
                        b.css({opacity: 1});
                        if (g.animateScroll && (w++ >= g.scrollPollIntervalInChars - 1 || a == y - 1)) {
                            w = 0;
                            0 == z && (z = h.height());
                            var c = g.scrollPadding + b.position().top - h.position().top -
                            z;
                            0 < c && u < c && (B.stop().animate({scrollTop: c}, 600), u = c)
                        }
                    })
                })(b, a(this))
            });
            var v = createjs.Tween.get(h);
            d.wait && (v = v.wait(d.wait));
            d.soundLoop && (v = v.call(function() {
                Sound.playSoundLoop(d.soundLoop, d.volume)
            }));
            f = 1;
            for (s = 0; s < r.length; s++) {
                if (p.hasOwnProperty(s))
                    if ("pause" == p[s].type) {
                        v = v.call(function() {
                            d.soundLoop && Sound.stopSound(d.soundLoop)
                        }).wait(p[s].value).call(function() {
                            d.soundLoop && Sound.playSoundLoop(d.soundLoop, d.volume)
                        }).call(r[s]);
                        continue
                    } else
                        "delay" == p[s].type && (f = "slow" == p[s].value ? 10 :
                        1);
                instantenaousTypeWriter == 1 ? v = v.call(r[s]) : v = v.wait(g.delay * f ).call(r[s])
            }
            v.call(function() {
                d.soundLoop && Sound.stopSound(d.soundLoop);
                "return-tween" != d.type && -1 != b.indexOf(v) && b.remove(v);
                g.callback && g.callback()
            });
            if ("return-tween" === d.type)
                return v;
            setTimeout(function() {
                b.push(v);
                if (g.speedUpOnClick)
                    a(window).on("click", k)
            }, g.delay)
        }
    })(jQuery);


})();